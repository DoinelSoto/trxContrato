package mx.isban.beans.contratos.contratosamparados;

public class BeanContratosAmparadosDAO {

	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String codEjecu;
	private String peNumPer;
	private String codCenCo;
	private String idBanca;
	private String fecAlta;
	public String getCoidEmpr() {
		return coidEmpr;
	}
	public void setCoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getCanalOpe() {
		return canalOpe;
	}
	public void setCanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getCanalCom() {
		return canalCom;
	}
	public void setCanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public String getIdEmpr() {
		return idEmpr;
	}
	public void setIdEmpr(String idEmpr) {
		this.idEmpr = idEmpr;
	}
	public String getIdCent() {
		return idCent;
	}
	public void setIdCent(String idCent) {
		this.idCent = idCent;
	}
	public String getIdContr() {
		return idContr;
	}
	public void setIdContr(String idContr) {
		this.idContr = idContr;
	}
	public String getCodEjecu() {
		return codEjecu;
	}
	public void setCodEjecu(String codEjecu) {
		this.codEjecu = codEjecu;
	}
	public String getPeNumPer() {
		return peNumPer;
	}
	public void setPeNumPer(String peNumPer) {
		this.peNumPer = peNumPer;
	}
	public String getCodCenCo() {
		return codCenCo;
	}
	public void setCodCenCo(String codCenCo) {
		this.codCenCo = codCenCo;
	}
	public String getIdBanca() {
		return idBanca;
	}
	public void setIdBanca(String idBanca) {
		this.idBanca = idBanca;
	}
	public String getFecAlta() {
		return fecAlta;
	}
	public void setFecAlta(String fecAlta) {
		this.fecAlta = fecAlta;
	}
	
	
	
}
