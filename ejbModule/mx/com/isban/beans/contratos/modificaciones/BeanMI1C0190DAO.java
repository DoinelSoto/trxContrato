package mx.com.isban.beans.contratos.modificaciones;

public class BeanMI1C0190DAO {

	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String tipConsu;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String idProd;
	private String idStiPro;
	private String idEmprCo;
	private String desAlias;
	private String CODESTAD;
	private String CODDETES;
	private String DESDETSE;
	private String FECAMBIO;
	public String getcoidEmpr() {
		return coidEmpr;
	}
	public void setcoidEmpr(String coidEmpr) {
		coidEmpr = coidEmpr;
	}
	public String getcanalOpe() {
		return canalOpe;
	}
	public void setcanalOpe(String canalOpe) {
		canalOpe = canalOpe;
	}
	public String getcanalCom() {
		return canalCom;
	}
	public void setcanalCom(String canalCom) {
		canalCom = canalCom;
	}
	public String gettipConsu() {
		return tipConsu;
	}
	public void settipConsu(String tipConsu) {
		tipConsu = tipConsu;
	}
	public String getidEmpr() {
		return idEmpr;
	}
	public void setidEmpr(String idEmpr) {
		idEmpr = idEmpr;
	}
	public String getidCent() {
		return idCent;
	}
	public void setidCent(String idCent) {
		idCent = idCent;
	}
	public String getidContr() {
		return idContr;
	}
	public void setidContr(String idContr) {
		idContr = idContr;
	}
	public String getidProd() {
		return idProd;
	}
	public void setidProd(String idProd) {
		idProd = idProd;
	}
	public String getidStiPro() {
		return idStiPro;
	}
	public void setidStiPro(String idStiPro) {
		idStiPro = idStiPro;
	}
	public String getidEmprCo() {
		return idEmprCo;
	}
	public void setidEmprCo(String idEmprCo) {
		idEmprCo = idEmprCo;
	}
	public String getdesAlias() {
		return desAlias;
	}
	public void setdesAlias(String desAlias) {
		desAlias = desAlias;
	}
	public String getCODESTAD() {
		return CODESTAD;
	}
	public void setCODESTAD(String cODESTAD) {
		CODESTAD = cODESTAD;
	}
	public String getCODDETES() {
		return CODDETES;
	}
	public void setCODDETES(String cODDETES) {
		CODDETES = cODDETES;
	}
	public String getDESDETSE() {
		return DESDETSE;
	}
	public void setDESDETSE(String dESDETSE) {
		DESDETSE = dESDETSE;
	}
	public String getFECAMBIO() {
		return FECAMBIO;
	}
	public void setFECAMBIO(String fECAMBIO) {
		FECAMBIO = fECAMBIO;
	}

	
}
