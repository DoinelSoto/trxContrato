package mx.com.isban.beans.contratos.traspasoejecutivos;

public class BeanTraspasoEjecutivosDAO {
	
	/*
	 * 
	 * Se condensan los beans de las transacciones
	 * 
	 * 22 . Nueva Transacción que permite realizar el traspaso entre ejecutivos y centro de costos. 
	 * 
	 * **/
	
	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String idProd;
	private String idStiPro;
	private String peNumPer;
	private String PeCalPar;
	private String codDocum;
	private String codEstNu;
	private String desComen;
	private String fecEstaD;
	public String getCoidEmpr() {
		return coidEmpr;
	}
	public void setCoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getCanalOpe() {
		return canalOpe;
	}
	public void setCanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getCanalCom() {
		return canalCom;
	}
	public void setCanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public String getIdEmpr() {
		return idEmpr;
	}
	public void setIdEmpr(String idEmpr) {
		this.idEmpr = idEmpr;
	}
	public String getIdCent() {
		return idCent;
	}
	public void setIdCent(String idCent) {
		this.idCent = idCent;
	}
	public String getIdContr() {
		return idContr;
	}
	public void setIdContr(String idContr) {
		this.idContr = idContr;
	}
	public String getIdProd() {
		return idProd;
	}
	public void setIdProd(String idProd) {
		this.idProd = idProd;
	}
	public String getIdStiPro() {
		return idStiPro;
	}
	public void setIdStiPro(String idStiPro) {
		this.idStiPro = idStiPro;
	}
	public String getPeNumPer() {
		return peNumPer;
	}
	public void setPeNumPer(String peNumPer) {
		this.peNumPer = peNumPer;
	}
	public String getPeCalPar() {
		return PeCalPar;
	}
	public void setPeCalPar(String peCalPar) {
		PeCalPar = peCalPar;
	}
	public String getCodDocum() {
		return codDocum;
	}
	public void setCodDocum(String codDocum) {
		this.codDocum = codDocum;
	}
	public String getCodEstNu() {
		return codEstNu;
	}
	public void setCodEstNu(String codEstNu) {
		this.codEstNu = codEstNu;
	}
	public String getDesComen() {
		return desComen;
	}
	public void setDesComen(String desComen) {
		this.desComen = desComen;
	}
	public String getFecEstaD() {
		return fecEstaD;
	}
	public void setFecEstaD(String fecEstaD) {
		this.fecEstaD = fecEstaD;
	}

	
	
	

}
