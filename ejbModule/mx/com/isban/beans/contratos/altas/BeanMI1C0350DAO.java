package mx.com.isban.beans.contratos.altas;

public class BeanMI1C0350DAO {

	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String tipConsu;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String idProd;
	private String idStiPro;
	private String idEmprCo;
	private String desAlias;
	private String TIPAGRUP;
	private String idCentAG;
	private String idContrAG;
	private String idProdAG;
	private String IDSTIPRAG;
	private String DESALIAG;
	public String getcoidEmpr() {
		return coidEmpr;
	}
	public void setcoidEmpr(String coidEmpr) {
		coidEmpr = coidEmpr;
	}
	public String getcanalOpe() {
		return canalOpe;
	}
	public void setcanalOpe(String canalOpe) {
		canalOpe = canalOpe;
	}
	public String getcanalCom() {
		return canalCom;
	}
	public void setcanalCom(String canalCom) {
		canalCom = canalCom;
	}
	public String gettipConsu() {
		return tipConsu;
	}
	public void settipConsu(String tipConsu) {
		tipConsu = tipConsu;
	}
	public String getidEmpr() {
		return idEmpr;
	}
	public void setidEmpr(String idEmpr) {
		idEmpr = idEmpr;
	}
	public String getidCent() {
		return idCent;
	}
	public void setidCent(String idCent) {
		idCent = idCent;
	}
	public String getidContr() {
		return idContr;
	}
	public void setidContr(String idContr) {
		idContr = idContr;
	}
	public String getidProd() {
		return idProd;
	}
	public void setidProd(String idProd) {
		idProd = idProd;
	}
	public String getidStiPro() {
		return idStiPro;
	}
	public void setidStiPro(String idStiPro) {
		idStiPro = idStiPro;
	}
	public String getidEmprCo() {
		return idEmprCo;
	}
	public void setidEmprCo(String idEmprCo) {
		idEmprCo = idEmprCo;
	}
	public String getdesAlias() {
		return desAlias;
	}
	public void setdesAlias(String desAlias) {
		desAlias = desAlias;
	}
	public String getTIPAGRUP() {
		return TIPAGRUP;
	}
	public void setTIPAGRUP(String tIPAGRUP) {
		TIPAGRUP = tIPAGRUP;
	}
	public String getidCentAG() {
		return idCentAG;
	}
	public void setidCentAG(String idCentAG) {
		idCentAG = idCentAG;
	}
	public String getidContrAG() {
		return idContrAG;
	}
	public void setidContrAG(String idContrAG) {
		idContrAG = idContrAG;
	}
	public String getidProdAG() {
		return idProdAG;
	}
	public void setidProdAG(String idProdAG) {
		idProdAG = idProdAG;
	}
	public String getIDSTIPRAG() {
		return IDSTIPRAG;
	}
	public void setIDSTIPRAG(String iDSTIPRAG) {
		IDSTIPRAG = iDSTIPRAG;
	}
	public String getDESALIAG() {
		return DESALIAG;
	}
	public void setDESALIAG(String dESALIAG) {
		DESALIAG = dESALIAG;
	}

	
	
}
