package mx.com.isban.beans.contratos.altas;

public class BeanMI1C0230DAO {
	
	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String codEjecu;
	private String peNumPer;
	private String codCenCo;
	private String idBanca;
	private String FECALTA;
	public String getcoidEmpr() {
		return coidEmpr;
	}
	public void setcoidEmpr(String coidEmpr) {
		coidEmpr = coidEmpr;
	}
	public String getcanalOpe() {
		return canalOpe;
	}
	public void setcanalOpe(String canalOpe) {
		canalOpe = canalOpe;
	}
	public String getcanalCom() {
		return canalCom;
	}
	public void setcanalCom(String canalCom) {
		canalCom = canalCom;
	}
	public String getidEmpr() {
		return idEmpr;
	}
	public void setidEmpr(String idEmpr) {
		idEmpr = idEmpr;
	}
	public String getidCent() {
		return idCent;
	}
	public void setidCent(String idCent) {
		idCent = idCent;
	}
	public String getidContr() {
		return idContr;
	}
	public void setidContr(String idContr) {
		idContr = idContr;
	}
	public String getcodEjecu() {
		return codEjecu;
	}
	public void setcodEjecu(String codEjecu) {
		codEjecu = codEjecu;
	}
	public String getpeNumPer() {
		return peNumPer;
	}
	public void setpeNumPer(String peNumPer) {
		peNumPer = peNumPer;
	}
	public String getcodCenCo() {
		return codCenCo;
	}
	public void setcodCenCo(String codCenCo) {
		codCenCo = codCenCo;
	}
	public String getidBanca() {
		return idBanca;
	}
	public void setidBanca(String idBanca) {
		idBanca = idBanca;
	}
	public String getFECALTA() {
		return FECALTA;
	}
	public void setFECALTA(String fECALTA) {
		FECALTA = fECALTA;
	}
	
	


}
