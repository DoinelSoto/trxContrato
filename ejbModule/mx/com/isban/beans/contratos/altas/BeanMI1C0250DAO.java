package mx.com.isban.beans.contratos.altas;

public class BeanMI1C0250DAO {

	private String codEstat;
	private String desEstat;
	private String indOpera;
	private String indBloq;
	private String indCance;
	private String indBloLe;
	private String IndInver;
	private String indValdo;
	private String indBlqble;
	private String IndCanbl;
	private String IndValdo;
	public String getcodEstat() {
		return codEstat;
	}
	public void setcodEstat(String codEstat) {
		codEstat = codEstat;
	}
	public String getdesEstat() {
		return desEstat;
	}
	public void setdesEstat(String desEstat) {
		desEstat = desEstat;
	}
	public String getindOpera() {
		return indOpera;
	}
	public void setindOpera(String indOpera) {
		indOpera = indOpera;
	}
	public String getindBloq() {
		return indBloq;
	}
	public void setindBloq(String indBloq) {
		indBloq = indBloq;
	}
	public String getindCance() {
		return indCance;
	}
	public void setindCance(String indCance) {
		indCance = indCance;
	}
	public String getindBloLe() {
		return indBloLe;
	}
	public void setindBloLe(String indBloLe) {
		indBloLe = indBloLe;
	}
	public String getIndInver() {
		return IndInver;
	}
	public void setIndInver(String IndInver) {
		IndInver = IndInver;
	}
	public String getindValdo() {
		return indValdo;
	}
	public void setindValdo(String indValdo) {
		indValdo = indValdo;
	}
	public String getindBlqble() {
		return indBlqble;
	}
	public void setindBlqble(String indBlqble) {
		indBlqble = indBlqble;
	}
	public String getIndCanbl() {
		return IndCanbl;
	}
	public void setIndCanbl(String IndCanbl) {
		IndCanbl = IndCanbl;
	}
	public String getIndValdo() {
		return IndValdo;
	}
	public void setIndValdo(String IndValdo) {
		IndValdo = IndValdo;
	}

	
}
