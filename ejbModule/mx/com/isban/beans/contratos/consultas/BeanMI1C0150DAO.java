package mx.com.isban.beans.contratos.consultas;

public class BeanMI1C0150DAO {
	
	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private int numFolio;
	private String codTipRe;
	
	public String getcoidEmpr() {
		return coidEmpr;
	}
	public void setcoidEmpr(String coidEmpr) {
		coidEmpr = coidEmpr;
	}
	public String getcanalOpe() {
		return canalOpe;
	}
	public void setcanalOpe(String canalOpe) {
		canalOpe = canalOpe;
	}
	public String getcanalCom() {
		return canalCom;
	}
	public void setcanalCom(String canalCom) {
		canalCom = canalCom;
	}
	public int getnumFolio() {
		return numFolio;
	}
	public void setnumFolio(int numFolio) {
		numFolio = numFolio;
	}
	public String getcodTipRe() {
		return codTipRe;
	}
	public void setcodTipRe(String codTipRe) {
		codTipRe = codTipRe;
	}

	
	
}
