package mx.com.isban.beans.contratos.consultas;

public class BeanMI1C0090DAO {

	private String codCatal;
	private String codValor;
	private String inDrella;
	private String codValRe;
	
	public String getCodCatal() {
		return codCatal;
	}
	public void setCodCatal(String codCatal) {
		this.codCatal = codCatal;
	}
	public String getCodValor() {
		return codValor;
	}
	public void setCodValor(String codValor) {
		this.codValor = codValor;
	}
	public String getInDrella() {
		return inDrella;
	}
	public void setInDrella(String inDrella) {
		this.inDrella = inDrella;
	}
	public String getCodValRe() {
		return codValRe;
	}
	public void setCodValRe(String codValRe) {
		this.codValRe = codValRe;
	}
	
	
	
}
