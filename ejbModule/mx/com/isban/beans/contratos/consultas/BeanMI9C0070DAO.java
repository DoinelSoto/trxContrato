package mx.com.isban.beans.contratos.consultas;

public class BeanMI9C0070DAO {

	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String codTiCli;
	private String codPerIn;
	private String codTipSe;
	private String inDrella;
	private String codTiClR;
	private String codPrIRe;
	private String codTipSR;
	
	public String getcoidEmpr() {
		return coidEmpr;
	}
	public void setcoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getcanalOpe() {
		return canalOpe;
	}
	public void setcanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getcanalCom() {
		return canalCom;
	}
	public void setcanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public String getCodTiCli() {
		return codTiCli;
	}
	public void setCodTiCli(String codTiCli) {
		this.codTiCli = codTiCli;
	}
	public String getCodPerIn() {
		return codPerIn;
	}
	public void setCodPerIn(String codPerIn) {
		this.codPerIn = codPerIn;
	}
	public String getCodTipSe() {
		return codTipSe;
	}
	public void setCodTipSe(String codTipSe) {
		this.codTipSe = codTipSe;
	}
	public String getInDrella() {
		return inDrella;
	}
	public void setInDrella(String inDrella) {
		this.inDrella = inDrella;
	}
	public String getCodTiClR() {
		return codTiClR;
	}
	public void setCodTiClR(String codTiClR) {
		this.codTiClR = codTiClR;
	}
	public String getCodPrIRe() {
		return codPrIRe;
	}
	public void setCodPrIRe(String codPrIRe) {
		this.codPrIRe = codPrIRe;
	}
	public String getCodTipSR() {
		return codTipSR;
	}
	public void setCodTipSR(String codTipSR) {
		this.codTipSR = codTipSR;
	}
	
	
	
	
}
