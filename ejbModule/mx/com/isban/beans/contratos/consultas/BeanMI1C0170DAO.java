package mx.com.isban.beans.contratos.consultas;

public class BeanMI1C0170DAO {

	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private String idEmpr;
	private String idCent;
	private String idContr;
	private String idProd;
	private String idStiPro;
	private String PeCalPar;
	private String peNumPer;
	private String codDocum;
	private String inDrella;
	private String idEmprR;
	private String idCentR;
	private String idContrR;
	private String idProdRR;
	private String idStiProR;
	private String PECALPAL;
	private String PENUMPEL;
	private String CODDOCUR;
	public String getcoidEmpr() {
		return coidEmpr;
	}
	public void setcoidEmpr(String coidEmpr) {
		coidEmpr = coidEmpr;
	}
	public String getcanalOpe() {
		return canalOpe;
	}
	public void setcanalOpe(String canalOpe) {
		canalOpe = canalOpe;
	}
	public String getcanalCom() {
		return canalCom;
	}
	public void setcanalCom(String canalCom) {
		canalCom = canalCom;
	}
	public String getidEmpr() {
		return idEmpr;
	}
	public void setidEmpr(String idEmpr) {
		idEmpr = idEmpr;
	}
	public String getidCent() {
		return idCent;
	}
	public void setidCent(String idCent) {
		idCent = idCent;
	}
	public String getidContr() {
		return idContr;
	}
	public void setidContr(String idContr) {
		idContr = idContr;
	}
	public String getidProd() {
		return idProd;
	}
	public void setidProd(String idProd) {
		idProd = idProd;
	}
	public String getidStiPro() {
		return idStiPro;
	}
	public void setidStiPro(String idStiPro) {
		idStiPro = idStiPro;
	}
	public String getPeCalPar() {
		return PeCalPar;
	}
	public void setPeCalPar(String PeCalPar) {
		PeCalPar = PeCalPar;
	}
	public String getpeNumPer() {
		return peNumPer;
	}
	public void setpeNumPer(String peNumPer) {
		peNumPer = peNumPer;
	}
	public String getcodDocum() {
		return codDocum;
	}
	public void setcodDocum(String codDocum) {
		codDocum = codDocum;
	}
	public String getinDrella() {
		return inDrella;
	}
	public void setinDrella(String inDrella) {
		inDrella = inDrella;
	}
	public String getidEmprR() {
		return idEmprR;
	}
	public void setidEmprR(String idEmprR) {
		idEmprR = idEmprR;
	}
	public String getidCentR() {
		return idCentR;
	}
	public void setidCentR(String idCentR) {
		idCentR = idCentR;
	}
	public String getidContrR() {
		return idContrR;
	}
	public void setidContrR(String idContrR) {
		idContrR = idContrR;
	}
	public String getidProdRR() {
		return idProdRR;
	}
	public void setidProdRR(String idProdRR) {
		idProdRR = idProdRR;
	}
	public String getidStiProR() {
		return idStiProR;
	}
	public void setidStiProR(String idStiProR) {
		idStiProR = idStiProR;
	}
	public String getPECALPAL() {
		return PECALPAL;
	}
	public void setPECALPAL(String pECALPAL) {
		PECALPAL = pECALPAL;
	}
	public String getPENUMPEL() {
		return PENUMPEL;
	}
	public void setPENUMPEL(String pENUMPEL) {
		PENUMPEL = pENUMPEL;
	}
	public String getCODDOCUR() {
		return CODDOCUR;
	}
	public void setCODDOCUR(String cODDOCUR) {
		CODDOCUR = cODDOCUR;
	}
	
	

}
