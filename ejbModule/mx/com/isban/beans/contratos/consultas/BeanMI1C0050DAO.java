package mx.com.isban.beans.contratos.consultas;

public class BeanMI1C0050DAO {
	
	private String coidEmpr;
	private String  canalOpe;
	private String  canalCom;
	private String  codEstat;
	private String  codSubEs;
	private String inDrella;
	private String codEstRe;
	private String codSubRe;
	
	public String getcoidEmpr() {
		return coidEmpr;
	}
	public void setcoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getcanalOpe() {
		return canalOpe;
	}
	public void setcanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getcanalCom() {
		return canalCom;
	}
	public void setcanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public String getcodEstat() {
		return codEstat;
	}
	public void setcodEstat(String codEstat) {
		this.codEstat = codEstat;
	}
	public String getcodSubEs() {
		return codSubEs;
	}
	public void setcodSubEs(String codSubEs) {
		this.codSubEs = codSubEs;
	}
	public String getInDrella() {
		return inDrella;
	}
	public void setInDrella(String inDrella) {
		this.inDrella = inDrella;
	}
	public String getCodEstRe() {
		return codEstRe;
	}
	public void setCodEstRe(String codEstRe) {
		this.codEstRe = codEstRe;
	}
	public String getCodSubRe() {
		return codSubRe;
	}
	public void setCodSubRe(String codSubRe) {
		this.codSubRe = codSubRe;
	}
	
	


}
