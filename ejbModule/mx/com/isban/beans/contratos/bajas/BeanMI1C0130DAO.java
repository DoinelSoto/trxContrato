package mx.com.isban.beans.contratos.bajas;

public class BeanMI1C0130DAO {

	private String coidEmpr;
	private String canalOpe;
	private String canalCom;
	private int numFolio;
	private String codTipRe;
	private int NuSecuen;
	
	public String getcoidEmpr() {
		return coidEmpr;
	}
	public void setcoidEmpr(String coidEmpr) {
		this.coidEmpr = coidEmpr;
	}
	public String getcanalOpe() {
		return canalOpe;
	}
	public void setcanalOpe(String canalOpe) {
		this.canalOpe = canalOpe;
	}
	public String getcanalCom() {
		return canalCom;
	}
	public void setcanalCom(String canalCom) {
		this.canalCom = canalCom;
	}
	public int getnumFolio() {
		return numFolio;
	}
	public void setnumFolio(int numFolio) {
		this.numFolio = numFolio;
	}
	public String getcodTipRe() {
		return codTipRe;
	}
	public void setcodTipRe(String codTipRe) {
		this.codTipRe = codTipRe;
	}
	public int getNuSecuen() {
		return NuSecuen;
	}
	public void setNuSecuen(int NuSecuen) {
		this.NuSecuen = NuSecuen;
	}

	
	
}
