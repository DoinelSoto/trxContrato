package mx.com.isban.beans.contratos.subestatuscontratos;

public class BeanSubEstatusContratosDAO {
	
	/*
	 * Se agregan los datos de entrada para las transacciones :
	 * 
	 * 28:
	 * codEstat
	   codSubEs
       desSubEs

	 * 29 :
	 * CODESTAT
	   CODSUBES

	 * 
	 * 
	 * 
	 * */

	private String codEstat;
	private String codSubEs;
    private String desSubEs;
    
	public String getCodEstat() {
		return codEstat;
	}
	public void setCodEstat(String codEstat) {
		this.codEstat = codEstat;
	}
	public String getCodSubEs() {
		return codSubEs;
	}
	public void setCodSubEs(String codSubEs) {
		this.codSubEs = codSubEs;
	}
	public String getDesSubEs() {
		return desSubEs;
	}
	public void setDesSubEs(String desSubEs) {
		this.desSubEs = desSubEs;
	}
	
	
}
